% Datos iniciales método multipaso
alpha = [-1, 0, 1];
beta = [3/2, 1/2, 0];
s = length(alpha);

% Cálculo polinomio característico
syms w z;
P = 0;
for j=1:s
    P = P + (alpha(j) - w * beta(j)) * z^(j-1);
end

% Cáculo raices polinomio
roots = solve(P,z);

% Región estabilidad
[x,y] = meshgrid(-6:0.01:6,-6:0.01:6);
l = x + 1j*y;

k = {};
for n=1:length(roots)
    ht = matlabFunction(roots(n));
    k{n} = abs(ht(l));
end

% Plot
hold on
ax = gca;
ax.XAxisLocation = 'origin';
ax.YAxisLocation = 'origin';
contourf(x,y,1-max(k{1},k{2}), [0 0], 'LineWidth', 1);
ss = sprintf('Región Estabilidad Absoluta AB2');
title(ss);
hold off